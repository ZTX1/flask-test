Projet Test avec Python sur le Framework FLASK
Pour faire fonctionner le projet, il faut utiliser MySQL et créer la base de donnée shayp
Puis créer la table tbl_user dont voici le code sql :

CREATE TABLE IF NOT EXISTS `tbl_user` (
  `user_id` bigint(20) NOT NULL AUTO_INCREMENT,
  `user_name` varchar(45) COLLATE utf8_unicode_ci DEFAULT NULL,
  `user_email` varchar(45) COLLATE utf8_unicode_ci DEFAULT NULL,
  `user_password` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  PRIMARY KEY (`user_id`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

Module installés:
- tables
- MySQL

Remarque:
Pas d'utilisation de regex pour confirmer que c'est un mail valide.
Le update est en post au lieu de put, pour plus de simplicité.
Le delete renvoie une page en get (mais supprimer en sql) au lieu de faire en delete.